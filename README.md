# Accessing attributes of Actors - Migrating from MSG to S4U

## Scenario:

There is a "main" Process (or Actor in the S4U API) that creates a child Process (or child Actor) that generates some information that will be consumed by the main Process. In order to access the child's information, in the Java code were created a getter method, for instance, ` result = child.getSomeValue() `, that is called from the main Process.

In Java, this returns the correct value. However, when I try to access the information of the child Actor in C++ using the S4U API, the correct value that was updated inside the "operator" method is not returned, it seems that the "scope" is different.


When I "print" the name of the Process inside the getter function in the child Process, it returns the name of the child Process, but when I do the same in the C++ version, it returns the name of the main Actor.


The main Process is the file *MasterVMManager.java*, and the child Process is the file *PVProcess.java*. The child method is *getCurrentGreenPowerProduction()*.

To run the Java version in Eclipse, it is necessary to import the jar file located into "lib/simgrid.jar". Project Properties -> Java Build Path -> Libraries -> Add External JARs ->  simgrid.jar file.


To run the C++  version, it is first necessary to compile the project, and then run it:

```
g++ SimulationMain.cpp ElectricDC.hpp ElectricDC.cpp PVActor.hpp PVActor.cpp -o app /usr/local/lib/libsimgrid.so.3.25 -I /opt/simgrid/include/
./app homogeneousGrid5000Pstate.xml
```


More about the files:
    
* *Main.java* -> Setups and starts the simulation
* *ElectricDC.java* -> A class used to compute the energy consumed by all servers in a data center.
* *PVProcess.java* that extends Process (from Msg API) -> A class used to inform the current green power production of the solar panels that a DC has. It reads a .csv file that contains photovoltaic traces
* *MasterVMManager.java* that extends Process (from Msg API) -> represents the master node that will have access to the information of energy consumption in the data centers.


## Output:

Here is the output for the Java version:

```
I start monitoring the green power production of DC pv_DC_A
OLD currentGreenPowerProduction
0.0
UPDATED currentGreenPowerProduction
0.0
GET currentGreenPowerProduction
pv_DC_A
0.0
GREEN PRODUCTION: 0.0
GET currentGreenPowerProduction
pv_DC_A
0.0
GREEN PRODUCTION: 0.0
OLD currentGreenPowerProduction
0.0
UPDATED currentGreenPowerProduction
0.0
new green power production for DC pv_DC_A : 0.0W
300.0
OLD currentGreenPowerProduction
0.0
UPDATED currentGreenPowerProduction
0.0
new green power production for DC pv_DC_A : 0.0W
600.0
GET currentGreenPowerProduction
pv_DC_A
0.0
GREEN PRODUCTION: 0.0
GET currentGreenPowerProduction
pv_DC_A
0.0
GREEN PRODUCTION: 0.0
OLD currentGreenPowerProduction
0.0
UPDATED currentGreenPowerProduction
0.0
new green power production for DC pv_DC_A : 0.0W
900.0
OLD currentGreenPowerProduction
0.0
UPDATED currentGreenPowerProduction
2.39024
new green power production for DC pv_DC_A : 2.39024W
1200.0
GET currentGreenPowerProduction
pv_DC_A
2.39024
GREEN PRODUCTION: 2.39024
GET currentGreenPowerProduction
pv_DC_A
2.39024
GREEN PRODUCTION: 2.39024
OLD currentGreenPowerProduction
2.39024
UPDATED currentGreenPowerProduction
4.76191
new green power production for DC pv_DC_A : 4.76191W
1500.0
OLD currentGreenPowerProduction
4.76191
UPDATED currentGreenPowerProduction
5.35714
new green power production for DC pv_DC_A : 5.35714W
1800.0
GET currentGreenPowerProduction
pv_DC_A
5.35714
GREEN PRODUCTION: 5.35714
GET currentGreenPowerProduction
pv_DC_A
5.35714
GREEN PRODUCTION: 5.35714
OLD currentGreenPowerProduction
5.35714
UPDATED currentGreenPowerProduction
7.63415
new green power production for DC pv_DC_A : 7.63415W
2100.0
OLD currentGreenPowerProduction
7.63415
UPDATED currentGreenPowerProduction
11.0714
new green power production for DC pv_DC_A : 11.0714W
2400.0
GET currentGreenPowerProduction
pv_DC_A
11.0714
GREEN PRODUCTION: 11.0714
GET currentGreenPowerProduction
pv_DC_A
11.0714
GREEN PRODUCTION: 11.0714
OLD currentGreenPowerProduction
11.0714
UPDATED currentGreenPowerProduction
14.561
new green power production for DC pv_DC_A : 14.561W
2700.0
GET currentGreenPowerProduction
pv_DC_A
14.561
GREEN PRODUCTION: 14.561
OLD currentGreenPowerProduction
14.561
UPDATED currentGreenPowerProduction
20.0294
new green power production for DC pv_DC_A : 20.0294W
3002.0

```


And here is the output for the C++ version:

```

[stremi-1.reims.grid5000.fr:simulation Main:(1) 0.000000] [edc/INFO] Electric DC created
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] I start monitoring the green power production of DC pv_DC_A
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] new green power production for DC pv_DC_A : 0.000000W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 0.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 300.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 300.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 300.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 300.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 300.000000] [pv/INFO] new green power production for DC pv_DC_A : 0.000000W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 300.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 302.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 302.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 302.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 302.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 302.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 600.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 600.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 600.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 600.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 600.000000] [pv/INFO] new green power production for DC pv_DC_A : 0.000000W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 600.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 602.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 602.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 602.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 602.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 602.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 900.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 900.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 900.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 900.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 900.000000] [pv/INFO] new green power production for DC pv_DC_A : 0.000000W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 900.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 902.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 902.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 902.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 902.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 902.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1200.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1200.000000] [pv/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1200.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1200.000000] [pv/INFO] 2.390240
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1200.000000] [pv/INFO] new green power production for DC pv_DC_A : 2.390240W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1200.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1202.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1202.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1202.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1202.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1202.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1500.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1500.000000] [pv/INFO] 2.390240
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1500.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1500.000000] [pv/INFO] 4.761910
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1500.000000] [pv/INFO] new green power production for DC pv_DC_A : 4.761910W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1500.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1502.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1502.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1502.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1502.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1502.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1800.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1800.000000] [pv/INFO] 4.761910
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1800.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1800.000000] [pv/INFO] 5.357140
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1800.000000] [pv/INFO] new green power production for DC pv_DC_A : 5.357140W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 1800.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1802.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1802.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1802.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1802.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 1802.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2100.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2100.000000] [pv/INFO] 5.357140
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2100.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2100.000000] [pv/INFO] 7.634150
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2100.000000] [pv/INFO] new green power production for DC pv_DC_A : 7.634150W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2100.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2102.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2102.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2102.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2102.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2102.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2400.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2400.000000] [pv/INFO] 7.634150
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2400.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2400.000000] [pv/INFO] 11.071400
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2400.000000] [pv/INFO] new green power production for DC pv_DC_A : 11.071400W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2400.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2402.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2402.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2402.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2402.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2402.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2700.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2700.000000] [pv/INFO] 11.071400
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2700.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2700.000000] [pv/INFO] 14.561000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2700.000000] [pv/INFO] new green power production for DC pv_DC_A : 14.561000W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 2700.000000] [pv/INFO] 302.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2702.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2702.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2702.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2702.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 2702.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3002.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3002.000000] [pv/INFO] 14.561000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3002.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3002.000000] [pv/INFO] 20.029400
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3002.000000] [pv/INFO] new green power production for DC pv_DC_A : 20.029400W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3002.000000] [pv/INFO] 300.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 3002.000000] [pv/INFO] GET currentGreenPowerProduction
[stremi-1.reims.grid5000.fr:simulation Main:(1) 3002.000000] [pv/INFO] simulation Main
[stremi-1.reims.grid5000.fr:simulation Main:(1) 3002.000000] [pv/INFO] 0.000000
GET currentGreenPowerProduction
0.000000
[stremi-1.reims.grid5000.fr:simulation Main:(1) 3002.000000] [edc/INFO] CURRENT POWER
[stremi-1.reims.grid5000.fr:simulation Main:(1) 3002.000000] [edc/INFO] 0.000000
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3302.000000] [pv/INFO] OLD currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3302.000000] [pv/INFO] 20.029400
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3302.000000] [pv/INFO] UPDATE currentGreenPowerProduction
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3302.000000] [pv/INFO] 21.634100
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3302.000000] [pv/INFO] new green power production for DC pv_DC_A : 21.634100W
[stremi-2.reims.grid5000.fr:pv_DC_A:(2) 3302.000000] [pv/INFO] 300.000000


```

