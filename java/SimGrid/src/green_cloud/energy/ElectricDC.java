package green_cloud.energy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import org.simgrid.msg.Host;

/**
 * The electric system of a single data-center.
 *
 */
public class ElectricDC {
	
	/**
	 * The name of the data-center.	
	 */
	private String id;
	
	/**
	 * The process monitoring the photo-voltaic power production of a single photo-voltaic panel in the data-center.
	 * (NB: we assume that all the photo-voltaic panels of a data-centers have the same production)
	 */
	private PVProcess pv;
	
	
	/**
	 * The number of photo-voltaic panel in the data-center.
	 * Used to scale the photo-voltaic power production.
	 */
	private int nbPV;
	
	/**
	 * The power output stream of the system.
	 */
	private BufferedWriter power_output;	
	
	
	/**
	 * Creates the electric system of a single data-center.
	 * @param anId The name of the data-center.
	 * @param host The host where the process monitoring the photo-voltaic power production of the data-center must run.
	 * @param inputFile the input file containing the power production trajectory of a single photo-voltaic panel in the data-center.
	 * @param aNbPV The number of photo-voltaic panels in the data-center. Used to scale the photo-voltaic power production.
	 * @param aMaster The master of the cloud.
	 */
	public ElectricDC(String anId, Host host, String inputFile, int aNbPV) { 
		id = anId;						
		pv = new PVProcess(host,"pv_"+anId,inputFile);						
		nbPV = aNbPV;				
		
		try {
			power_output = new BufferedWriter(new FileWriter("output/power_"+anId+".csv"));
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e);
			System.out.println(e.getMessage());
			System.out.println(new Date());
		}
		
	}
		
	/**
	 * Stop the system -i.e. close the output stream and the process monitoring the photo-voltaic power production
	 * (called by the master at the end of the simulation).
	 */
	public void stop(){
		try {
			power_output.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(e);
			System.out.println(e.getMessage());
			System.out.println(new Date());
		}
		pv.kill();
	}	

	public void startPvProcess() {
		pv.start();		
	}
	
	/**
	 * Return the current local power production of the DC.
	 * Please note that this production is differ from the power available for consumption because it do NOT consider power I/O migrations.
	 * @return The current local power production of the DC.
	 */
	public double getLocalPowerProduction(){				
		double greenEnergy = pv.getCurrentGreenPowerProduction();
		System.out.println("GREEN PRODUCTION: "+greenEnergy);		
		return greenEnergy * nbPV;		
	}
	
}
