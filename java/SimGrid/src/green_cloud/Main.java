package green_cloud;

import org.simgrid.msg.Host;
import org.simgrid.msg.HostNotFoundException;
import org.simgrid.msg.Msg;
import green_cloud.master_hosts.MasterVMManager;

public class Main {
	
	/**
	 * The SimGrid XML file of the cloud platform description.
	 */
	private final static String platformFile = "input/platform/homogeneousGrid5000Pstate.xml";

	/**
	 * The trace level desired in SimGrid log (see SimGrid for more information).
	 */
	private final static String traceLevel = "info";
	
	/**
	 * The file where SimGrid log must be written
	 */
	private final static String outputLogFile = "output/simgridresult.log";
	
	
	/**
	 * Launch the simulation.
	 * @param args Not used.
	 * @throws HostNotFoundException
	 */
	public static void main(String[] args) throws HostNotFoundException {		
		Msg.energyInit();		
		Msg.init(new String[]{"--log=root.threshold:"+traceLevel,"--log=root.app:file:"+outputLogFile});		
		Msg.createEnvironment(platformFile);				
	    new MasterVMManager(Host.all()[0]).start();		
		Msg.run();
		
	}
}