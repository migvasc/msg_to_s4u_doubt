package green_cloud.master_hosts;

import org.simgrid.msg.Host;
import org.simgrid.msg.HostFailureException;
import org.simgrid.msg.HostNotFoundException;
import org.simgrid.msg.MsgException;
import org.simgrid.msg.Process;
import green_cloud.energy.ElectricDC;


public class MasterVMManager extends Process {
	
	public static String FILE_SEPARATOR = ";";
	
	public MasterVMManager (Host host) throws HostNotFoundException  {
	    super(host, "master");
	 }
			
	@Override
	/**
	 * Behavior of the master. It contains the main simulation loop.
	 */
	public void main(String[] arg0) throws MsgException, HostNotFoundException, HostFailureException { 
		
		ElectricDC a = new ElectricDC("DC_A", getHost(), "input/photo-voltaic/winter.csv", 1);
	    a.startPvProcess();
	
	    for(int i =0; i<=10; i++) {
	        a.getLocalPowerProduction(); 
	        waitFor(300);
	    }
	    
	    a.stop();
	}
	
}