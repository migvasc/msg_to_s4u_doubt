#include "PVActor.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>  
#include <stdlib.h>
#include <simgrid/s4u.hpp>

using namespace std;


/**
 * 
 * The electric system of a single data-center.
 *
 */
class ElectricDC
{

private:	
	/**
	 * The name of the data-center.	
	 */
	string id;
	
	/**
	 * The number of photo-voltaic panel in the data-center.
	 * Used to scale the photo-voltaic power production.
	 */
	int nbPV;
	

	/**
	 * The power output stream of the system.
	 */
	ofstream power_output;
	

	
			


	

public:
	
	/**
	 * The process monitoring the photo-voltaic power production of a single photo-voltaic panel in the data-center.
	 * (NB: we assume that all the photo-voltaic panels of a data-centers have the same production)
	 */
     PVActor pv;	

    /**
     * Pointer to the PV actor, used to kill, suspend and resume the actor
     * */
    simgrid::s4u::ActorPtr pv_actor_ptr;



    ElectricDC() ;
	/**
	 * Creates the electric system of a single data-center.
	 * @param anId The name of the data-center.
	 * @param host The host where the process monitoring the photo-voltaic power production of the data-center must run.
	 * @param inputFile the input file containing the power production trajectory of a single photo-voltaic panel in the data-center.
	 * @param aNbPV The number of photo-voltaic panels in the data-center. Used to scale the photo-voltaic power production.

	 */
	 ElectricDC(string anId,                 
                string inputFile,
				string _hostName,
                int aNbPV );

    
	/**
	 * Return the power currently available for the local consumption.
	 * It is equals to: local_production + input_migration - output_migration
	 * @return The current production of the data-center.
	 */
	
    /**
	 * Return the current local power production of the DC.
	 * Please note that this production is differ from the power available for consumption because it do NOT consider power I/O migrations.
	 * @return The current local power production of the DC.
	 */
	double getLocalPowerProduction();
	

		
	string getName();
    string hostName;
	virtual void startPvProcess() ;
    virtual void stop();
	

};
