
#include "ElectricDC.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>  
#include <stdlib.h>
#include <simgrid/s4u.hpp>

XBT_LOG_NEW_DEFAULT_CATEGORY(edc, "edc log");

        
/**
 * Return the current local power production of the DC.
 * Please note that this production is differ from the power available for consumption because it do NOT consider power I/O migrations.
 * @return The current local power production of the DC.
 */
double ElectricDC::getLocalPowerProduction(){
    double _currentGreenPowerProduction =  pv.getCurrentGreenPowerProduction();
    simgrid::s4u::Actor::by_pid(pv_actor_ptr->get_pid());     
    XBT_INFO("CURRENT POWER");
    XBT_INFO(to_string(_currentGreenPowerProduction).c_str());
    return  _currentGreenPowerProduction * nbPV;
}

//ElectricDC::~ElectricDC(){};

ElectricDC::ElectricDC()  = default;

/**
 * Creates the electric system of a single data-center.
 * @param anId The name of the data-center.
 * @param host The host where the process monitoring the photo-voltaic power production of the data-center must run.
 * @param inputFile the input file containing the power production trajectory of a single photo-voltaic panel in the data-center.
 * @param aNbPV The number of photo-voltaic panels in the data-center. Used to scale the photo-voltaic power production.
*/
    ElectricDC::ElectricDC(string anId,                 
            string inputFile,
            string _hostName,
            int aNbPV ) 
            { 
    
    pv = PVActor(inputFile);
    string name = "pv_"+anId;
    hostName=_hostName;
    pv_actor_ptr=
        simgrid::s4u::Actor::create(name, simgrid::s4u::Host::by_name(hostName), pv);
    id = anId;														
    nbPV = aNbPV;	

    try {        
        power_output.open(("output/power_"+anId+".csv"));
    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what()   << endl;
    }			
    XBT_INFO("Electric DC created");

}


/**
 * Stop the system -i.e. close the output stream and the process monitoring the photo-voltaic power production
 * (called by the master at the end of the simulation).
 */

void ElectricDC::stop(){
    //super.stop();
    try {
        power_output.close();
    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what()   << endl;
    }    	
    //pv.kill();
}


void ElectricDC::startPvProcess() {
    pv_actor_ptr->resume();
}

string ElectricDC::getName(){
    return simgrid::s4u::this_actor::get_cname();
}


    