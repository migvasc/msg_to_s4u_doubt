#include "simgrid/s4u.hpp"
#include <string>
#include "ElectricDC.hpp"

void simulationMain()
{
    ElectricDC a  = ElectricDC("DC_A","winter.csv","stremi-1.reims.grid5000.fr",1);    
    a.startPvProcess(); 
    simgrid::s4u::this_actor::sleep_for(2);			
    for(int i =0; i<= 10;i++){
        a.getLocalPowerProduction();
        simgrid::s4u::this_actor::sleep_for(300);			
    }

    a.stop();
    simgrid::s4u::Actor::kill_all();  
    simgrid::s4u::this_actor::exit();

}

int main(int argc, char* argv[])
{
    simgrid::s4u::Engine e(&argc, argv);
    e.load_platform(argv[1]); /* - Load the platform description */  
    simgrid::s4u::Actor::create("simulation Main",
    simgrid::s4u::Host::by_name("stremi-1.reims.grid5000.fr"), simulationMain);
    e.run();   
    return 0;

}