#include <iostream>
#include <fstream>
#include <simgrid/s4u.hpp>
#include <string>
#include <boost/algorithm/string.hpp>
#include <stdlib.h>
#include <limits>
#include "PVActor.hpp"


XBT_LOG_NEW_DEFAULT_CATEGORY(pv, "pv log");

 PVActor::PVActor() = default;
 
 PVActor::PVActor(string inputFile_){		
    inputFile = inputFile_;		
} 

void PVActor::operator()() {     
    simgrid::s4u::this_actor::suspend();
    string logInfo = "I start monitoring the green power production of DC " +getName();
    XBT_INFO(logInfo.c_str());
    ifstream input;
    try {

        input =  ifstream(inputFile);
        advanceTime(&input);  
        time = 0;
        currentGreenPowerProduction = nextGreenPowerProduction;

        while(nextTime != numeric_limits<double>::max() ){               
            double sleep_amount = nextTime - time;                        
            XBT_INFO(to_string(sleep_amount).c_str());
            simgrid::s4u::this_actor::sleep_for(sleep_amount);			
            advanceTime(&input);
            logInfo = "new green power production for DC "+getName()+" : "+ to_string(currentGreenPowerProduction)+"W";
            XBT_INFO(logInfo.c_str());            
        }    
        input.close();            

    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what() << endl;

    }	
    
}

/**
 * Return the current green power production according to the input trace and the power migrating from/to this data-center.
 * @return The current green power production.
 */
double PVActor::getCurrentGreenPowerProduction() {
    XBT_INFO("GET currentGreenPowerProduction");
    XBT_INFO(simgrid::s4u::this_actor::get_cname());
    XBT_INFO(to_string(currentGreenPowerProduction).c_str());
    std::cout << "GET currentGreenPowerProduction" << endl;
    std::cout << to_string(currentGreenPowerProduction).c_str() << endl;
    return currentGreenPowerProduction;
}

/**
 * Advance to the new update of the power production -i.e. change the current/next time/production according to the trace.
 */
void PVActor::advanceTime(ifstream *input) {
    
    try {
        string  nextLine;        
        getline(*input,nextLine);
        if(!nextLine.empty())
        {            
            XBT_INFO("OLD currentGreenPowerProduction");
            XBT_INFO(to_string(currentGreenPowerProduction).c_str());
            currentGreenPowerProduction = nextGreenPowerProduction;
            XBT_INFO("UPDATE currentGreenPowerProduction");
            XBT_INFO(to_string(currentGreenPowerProduction).c_str());
            std::vector<std::string> nextInput;				
            // TODO: GET FROM THIS FILE THE SEPARATOR
            // std::string nextInput ;//= nextLine.split(MasterVMManager.FILE_SEPARATOR);
            boost::algorithm::split(nextInput, nextLine, boost::is_any_of(";"));
            nextTime = atof(nextInput[0].c_str());
            nextGreenPowerProduction = atof(nextInput[1].c_str());
            time = simgrid::s4u::Engine::get_clock();				            
        }
        else 
        {
            nextTime = numeric_limits<double>::max();
        }

    } catch (const std::exception& e) 
    {            
        cout << "Error : " << e.what() << endl;

    }			
}

string PVActor::getName(){
    return simgrid::s4u::this_actor::get_cname();
}

